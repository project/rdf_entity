<?php

declare(strict_types = 1);

namespace Drupal\rdf_taxonomy\Tests;

use Drupal\Tests\rdf_entity\Kernel\RdfKernelTestBase;
use Drupal\rdf_taxonomy\Entity\RdfTerm;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;

/**
 * Tests Entity Query functionality of the Sparql backend.
 *
 * @see \Drupal\KernelTests\Core\Entity\EntityQueryTest
 *
 * @group Entity
 */
class SparqlTaxonomyTest extends RdfKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field_test',
    'language',
    'rdf_taxonomy',
    'rdf_taxonomy_test',
    'taxonomy',
  ];

  /**
   * A list of query results.
   *
   * @var array
   */
  protected $queryResults;

  /**
   * A list of bundle machine names created for this test.
   *
   * @var string[]
   */
  protected $bundles;

  /**
   * Dummy reference entities.
   *
   * @var \Drupal\rdf_entity\RdfInterface[]
   */
  protected $dummyEntities;

  /**
   * The entity reference field.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['rdf_taxonomy_test']);

    $vocabulary = 'taxonomy_test';
    $prefix = "http://$vocabulary/";
    for ($i = 0; $i < 3; $i++) {
      $id = sprintf("%s%03d", $prefix, $i + 1);
      $values = [
        'tid' => $id,
        'vid' => $vocabulary,
        'name' => $this->randomMachineName(),
        'description' => $this->randomString(255),
      ];
      Term::create($values)->save();
    }

    $results = $this->container->get('entity_type.manager')
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->condition('vid', 'taxonomy_test')
      ->execute();
    $this->assertCount($i, $results, "${i} terms were loaded successfully.");
  }

  /**
   * Tests that a term insert saves the proper values.
   */
  public function testTaxonomyInsert(): void {
    $values = [
      'tid' => 'http://taxonomy_test/004',
      'vid' => 'taxonomy_test',
      'name' => $this->randomMachineName(),
      'description' => $this->randomString(255),
    ];

    $term = Term::create($values);
    $term->save();
    $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $term = $taxonomy_storage->loadUnchanged($term->id());
    foreach ($values as $field => $expected_value) {
      $actual_value = $term->get($field)->first()->getValue();
      $actual_value = is_array($actual_value) ? reset($actual_value) : $actual_value;
      $this->assertEquals($expected_value, $actual_value);
    }

    $terms = $taxonomy_storage->loadByProperties([
      'vid' => 'taxonomy_test',
    ]);
    $this->assertCount(4, $terms);
  }

  /**
   * Tests that an entity can reference a taxonomy and can be queried normally.
   */
  public function testTaxonomyReference() {
    $storage = $this->container->get('entity_type.manager')->getStorage('rdf_entity');
    $entity_multi_label = $this->randomMachineName();
    $storage->create([
      'label' => $entity_multi_label,
      'rid' => 'dummy',
      'field_taxonomy' => [
        'http://taxonomy_test/003',
      ],
    ])->save();

    $results = $storage->getQuery()
      ->condition('field_taxonomy', 'http://taxonomy_test/003')
      ->execute();
    $id = reset($results);
    $entity = $storage->load($id);

    $actual_value = $entity->get('field_taxonomy')->first()->target_id;
    $this->assertSame('http://taxonomy_test/003', $actual_value);
  }

  /**
   * Tests term parent reference.
   */
  public function testParent(): void {
    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = $this->container->get('entity_type.manager')->getStorage('taxonomy_term');

    // Test a term whose vocabulary doesn't map the parent.target_id column.
    RdfTerm::create([
      'vid' => 'no_parent',
      'tid' => 'http://example.com/no-parent',
      'name' => $this->randomString(),
    ])->save();
    $no_parent_term = RdfTerm::load('http://example.com/no-parent');
    // Even the parent field is not mapped, the Drupal field API correctly
    // returns the '0' pseudo-reference while no triple is stored in SPARQL.
    $this->assertFalse($no_parent_term->get('parent')->isEmpty());
    $this->assertParentIds(['0'], $no_parent_term);
    $this->assertNoParentTriple($no_parent_term);
    // This method doesn't load the '0' pseudo-reference.
    $this->assertEmpty($storage->loadParents($no_parent_term->id()));
    // This method loads also the term itself but not the '0' pseudo-reference.
    $this->assertSame(['http://example.com/no-parent'], array_keys($storage->loadAllParents($no_parent_term->id())));
    $this->assertEquals([
      (object) [
        'tid' => $no_parent_term->id(),
        'vid' => 'no_parent',
        'name' => $no_parent_term->label(),
        'weight' => 0,
        'depth' => 0,
        'parents' => [0],
      ],
    ], $storage->loadTree('no_parent', '0'));

    // Top-level term with no other parent.
    $term = RdfTerm::load('http://taxonomy_test/001');
    $this->assertFalse($term->get('parent')->isEmpty());
    $this->assertCount(1, $term->get('parent'));
    $this->assertParentIds(['0'], $term);
    $this->assertParentTriples(['http://example.com/rdf_taxonomy/test'], $term);
    // This method doesn't load the '0' pseudo-reference.
    $this->assertEmpty($storage->loadParents($term->id()));
    // This method loads also the term itself but not the '0' pseudo-reference.
    $this->assertSame(['http://taxonomy_test/001'], array_keys($storage->loadAllParents($term->id())));
    $this->assertCount(3, $storage->loadTree('taxonomy_test', '0'));

    // Term with multiple, non-root parents.
    $parents = ['http://taxonomy_test/002', 'http://taxonomy_test/003'];
    $term->set('parent', $parents)->save();
    // The storage statically caches the vocabulary tree structure.
    $storage->resetCache();
    $this->assertFalse($term->get('parent')->isEmpty());
    $this->assertCount(2, $term->get('parent'));
    $this->assertParentIds($parents, $term);
    $this->assertParentTriples($parents, $term);
    // This method doesn't load the '0' pseudo-reference.
    $this->assertEqualsCanonicalizing($parents, array_keys($storage->loadParents($term->id())));
    // This method loads also the term itself.
    $this->assertEqualsCanonicalizing([
      'http://taxonomy_test/001',
      'http://taxonomy_test/002',
      'http://taxonomy_test/003',
    ], array_keys($storage->loadAllParents($term->id())));
    // A term with multiple parents appears multiple times in the tree.
    $this->assertCount(4, $storage->loadTree('taxonomy_test', '0'));

    // Mixed, root and non-root parents.
    $parents = ['0', 'http://taxonomy_test/002', 'http://taxonomy_test/003'];
    $term->set('parent', $parents)->save();
    // The storage statically caches the vocabulary tree structure.
    $storage->resetCache();
    $this->assertFalse($term->get('parent')->isEmpty());
    $this->assertCount(3, $term->get('parent'));
    $this->assertParentIds($parents, $term);
    $this->assertParentTriples([
      'http://example.com/rdf_taxonomy/test',
      'http://taxonomy_test/002',
      'http://taxonomy_test/003',
    ], $term);
    // This method doesn't load the '0' pseudo-reference.
    $this->assertEqualsCanonicalizing([
      'http://taxonomy_test/002',
      'http://taxonomy_test/003',
    ], array_keys($storage->loadParents($term->id())));
    // This method loads also the term itself but not the '0' pseudo-reference.
    $this->assertEqualsCanonicalizing([
      'http://taxonomy_test/001',
      'http://taxonomy_test/002',
      'http://taxonomy_test/003',
    ], array_keys($storage->loadAllParents($term->id())));
    // A term with multiple parents appears multiple times in the tree.
    $this->assertCount(5, $storage->loadTree('taxonomy_test', '0'));
  }

  /**
   * Asserts that a list of parent references are retrieved from the SPARQL.
   *
   * @param string[] $parents
   *   A list of expected parents (including the '0' pseudo-reference).
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to be checked.
   */
  protected function assertParentTriples(array $parents, TermInterface $term): void {
    $this->assertEqualsCanonicalizing($parents, $this->getParentTriples($term));
  }

  /**
   * Asserts that no parent triple has been stored in SPARQL.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to be checked.
   */
  protected function assertNoParentTriple(TermInterface $term): void {
    $this->assertEmpty($this->getParentTriples($term));
  }

  /**
   * Asserts that a list of parents are retrieved from Drupal entity API.
   *
   * @param string[] $parents
   *   A list of expected parents (including the '0' pseudo-reference).
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to be checked.
   */
  protected function assertParentIds(array $parents, TermInterface $term): void {
    $actual_parents = array_map(function (array $item): string {
      return $item['target_id'];
    }, $term->get('parent')->getValue());
    $this->assertEqualsCanonicalizing($parents, $actual_parents);
  }

  /**
   * Returns a list of parent triple values given an RDF term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to be checked.
   *
   * @return string[]
   *   A list of parent references including the '0' pseudo-reference, if case.
   */
  protected function getParentTriples(TermInterface $term): array {
    $query = <<<QUERY
    SELECT ?parent
    WHERE {
      GRAPH <http://example.com/rdf_taxonomy/published> {
        <{$term->id()}> <http://www.w3.org/2004/02/skos/core#broader> ?parent .
      }
    }
    QUERY;

    $values = [];
    foreach ($this->sparql->query($query) as $row) {
      $values[] = (string) $row->parent;
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {
    // Delete all data produced by testing module.
    foreach (['published', 'draft'] as $graph) {
      $query = <<<EndOfQuery
DELETE {
  GRAPH <http://example.com/rdf_taxonomy/$graph> {
    ?entity ?field ?value
  }
}
WHERE {
  GRAPH <http://example.com/rdf_taxonomy/$graph> {
    ?entity ?field ?value
  }
}
EndOfQuery;
      $this->sparql->query($query);
    }

    parent::tearDown();
  }

}
