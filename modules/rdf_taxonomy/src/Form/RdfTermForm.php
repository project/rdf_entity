<?php

declare(strict_types = 1);

namespace Drupal\rdf_taxonomy\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sparql_entity_storage\Entity\SparqlMapping;
use Drupal\taxonomy\TermForm;
use Drupal\taxonomy\TermInterface;

/**
 * Alternative entity form class for RDF taxonomy term.
 */
class RdfTermForm extends TermForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = $this->getEntity();

    $sparql_mapping = SparqlMapping::loadByName('taxonomy_term', $term->bundle());

    // Terms in vocabularies not mapping the parent.target_id field column.
    $is_mapped_parent = $sparql_mapping->isMapped('parent', 'target_id');
    if (!$is_mapped_parent) {
      $form['relations']['parent']['#access'] = FALSE;
    }
    $is_mapped_weight = $sparql_mapping->isMapped('weight');
    if (!$is_mapped_weight) {
      // RDF terms with no weight mapping have 0 weight.
      $form['relations']['weight'] = [
        '#type' => 'value',
        '#value' => 0,
      ];
    }

    // RDF terms have no language. Set the entity default language to prevent a
    // form validation error.
    $form['langcode']['widget'][0]['value']['#default_value'] = $term->language()->getId();

    if ($is_mapped_parent || $is_mapped_weight) {
      // Visual enhancements.
      $form['relations']['#open'] = TRUE;
    }
    else {
      $form['relations']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getParentIds(TermInterface $term): array {
    // Override the parent method to avoid casting the parent ID to an integer.
    // @see https://www.drupal.org/i/3332877
    // @see https://www.drupal.org/i/3332666
    $parent = [];
    // Get the parent directly from the term as
    // \Drupal\taxonomy\TermStorageInterface::loadParents() excludes the root.
    foreach ($term->get('parent') as $item) {
      $parent[] = $item->target_id;
    }
    return $parent;
  }

}
