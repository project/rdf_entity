<?php

declare(strict_types = 1);

namespace Drupal\rdf_taxonomy\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\sparql_entity_storage\Event\InboundValueEvent;
use Drupal\sparql_entity_storage\Event\OutboundValueEvent;
use Drupal\sparql_entity_storage\Event\SparqlEntityStorageEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Massages parent term parent reference.
 *
 * Drupal core refers the parent root using the '0' pseudo-reference. RDF terms
 * are storing such terms using a triple referencing the term vocabulary RDF
 * type, in order to satisfy the value type of parent's predicates:
 * - http://www.w3.org/2004/02/skos/core#broader
 * - http://www.w3.org/2004/02/skos/core#broaderTransitive
 * This event subscriber listens to the inbound and outbound parent term values
 * computing and is doing the conversion.
 */
class TermParentSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new event subscriber instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SparqlEntityStorageEvents::OUTBOUND_VALUE => 'literalToUri',
      SparqlEntityStorageEvents::INBOUND_VALUE => 'uriToLiteral',
    ];
  }

  /**
   * Converts the term parent ID from '0' to vocabulary's RDF type.
   *
   * @param \Drupal\sparql_entity_storage\Event\OutboundValueEvent $event
   *   The outbound value event.
   */
  public function literalToUri(OutboundValueEvent $event): void {
    if ($event->getEntityTypeId() === 'taxonomy_term' && $event->getField() === 'parent' && $event->getValue() == 0) {
      /** @var \Drupal\sparql_entity_storage\SparqlMappingInterface $mapping */
      if (!$mapping = $this->entityTypeManager->getStorage('sparql_mapping')->load("{$event->getEntityTypeId()}.{$event->getBundle()}")) {
        return;
      }

      if ($mapping->isMapped('parent', 'target_id')) {
        // Convert '0' to the vocabulary RDF type as the parent predicate could
        // be one of:
        // - http://www.w3.org/2004/02/skos/core#broaderTransitive
        // - http://www.w3.org/2004/02/skos/core#broader
        // and both are requiring a resource/URI as object type.
        $event->setValue($mapping->getRdfType());
      }
    }
  }

  /**
   * Converts the term parent ID from vocabulary's RDF type to '0'.
   *
   * @param \Drupal\sparql_entity_storage\Event\InboundValueEvent $event
   *   The inbound value event.
   */
  public function uriToLiteral(InboundValueEvent $event): void {
    if ($event->getEntityTypeId() === 'taxonomy_term' && $event->getField() === 'parent') {
      /** @var \Drupal\sparql_entity_storage\SparqlMappingInterface $mapping */
      $mapping = $this->entityTypeManager->getStorage('sparql_mapping')->load("{$event->getEntityTypeId()}.{$event->getBundle()}");
      if ($mapping->isMapped('parent', 'target_id') && $event->getValue() === $mapping->getRdfType()) {
        // Convert the vocabulary RDF type to '0' to satisfy Drupal field API.
        $event->setValue('0');
      }
    }
  }

}
